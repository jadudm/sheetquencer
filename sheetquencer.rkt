#lang racket
(require data-table)

(require rsound)
 (require rsound/piano-tones)


(define sounds (make-hash))
(define (load-sounds dir)
  (for ([file (directory-list dir)])
    (when (regexp-match "wav$" (format "~a" file))
      (printf "Reading: ~a~n" file)
      (hash-set! (format "~a" (regexp-replace "\\.wav" (~a file) ""))
                 (rs-read file)))))

(define bpm 100)
(define (set-bpm! v)
  (set! bpm v))

(define (seq loop)
  (cond
    [(empty? loop) 'done]
    [else
     (match (first loop)
       [(? rsound? snd)
        (play (first loop))]
       [else 'skip])
     (sleep (/ 60 (* bpm 8)))
     (seq (rest loop))]))

(define s1
  (list kick 0 0 0 kick 0 0 0 kick 0 0 0  kick 0 0 0))

(define s2
  (list 0 clap-1 clap-1 0 0 clap-1 clap-1 0 0 clap-1 clap-1 0 0 clap-1 clap-1 clap-1))
     

(define-syntax-rule (repeat n body)
  (let loop ([m n])
    (unless (zero? m)
      body
    (loop (sub1 m)))))

(define (weird)
  (begin
    (thread (thunk
             (repeat 16 (let ()
                          (play (piano-tone (+ 64 (random 16))))
                          (sleep 0.25)))))
    (thread
     (thunk
      (repeat 8 (let ()
                  (play (piano-tone (+ 32 (random 16))))
                  (sleep 0.5)))))
    (thread
     (thunk
      (repeat 4 (let ()
                  (play (piano-tone (+ 16 (random 16))))
                  (sleep 1)))))

    (thread
     (thunk
      (repeat 32 (let ()
                  (play (piano-tone (+ 80 (random 32))))
                  (sleep 0.125)))))
    ))
