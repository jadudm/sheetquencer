s.boot;

NetAddr.localAddr

(
OSCdef.new(
	{|msg, time, addr, port|
		"X: ".postln;
	},
	"/mouse-x"
);
)

(
OSCdef.new(
	{|msg, time, addr, port|
		// a simple function that triggers an envelope
        {Pulse.ar(1000,rrand(0.01,0.5),0.3)!2 * EnvGen.ar(Env.perc,doneAction:2)}.play},
	"/click");
)

/*
// { [SinOsc.ar(440, 0, 0.2), SinOsc.ar(442, 0, 0.2)] }.play;

n = NetAddr.new("127.0.0.1", 12000);
o = OSCFunc({ |msg, time, addr, recvPort| [msg, time, addr, recvPort].postln; }, '/goodbye', n);

var mx, my;

var mx = OSCFunc({ |msg, time, addr, recvPort| [msg, time, addr, recvPort].postln; }, '/mouse-x', n);
var my = OSCFunc({ |msg, time, addr, recvPort| [msg, time, addr, recvPort].postln; }, '/mouse-y', n);



// this example is basically like OSCFunc.trace but filters out
// /status.reply messages
(
f = { |msg, time, addr|
    if(msg[0] != '/status.reply') {
        "time: % sender: %\nmessage: %\n".postf(time, addr, msg);
    }
};
thisProcess.addOSCRecvFunc(f);
);

// stop posting.
thisProcess.removeOSCRecvFunc(f);
o.free;
*/